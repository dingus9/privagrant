# Privagrant

Your private vagrant cloud

## Install dev environment

`pip install -e .`

## Run dev server

```
docker run -d \
  -v $PWD/data/db:/data/db \
  -p 27017:27017 \
  mongo:4.4.0-bionic
```

`python index.py`

## Testing

`tox -e unittest`

### Manual querying

#### Authentication

```
export VAGRANT_SERVER_URL=http://localhost:8080
vagrant cloud auth login

curl -H"Authorization: Bearer $(cat ~/.vagrant.d/data/vagrant_login_token)" http://localhost:8080/api/v1/authenticate

# OR

curl -XPOST -d '{"user": {"login": "user", "password": "testpass"}, "token": {"description": "test-token"}, "two_factor": {"code": null}}' http://localhost:8080/api/v1/authenticate

```

##### Queries

```
curl -H"Authorization: Bearer $(cat ~/.vagrant.d/data/vagrant_login_token)" http://localhost:8080/api/v1/boxes
```

## Notes About API

- Dates: timestamp is an iso8601 formatted date
