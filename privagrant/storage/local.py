import os
import hashlib


class LocalStorage:

    def __init__(self, config):
        self.base_dir = resolve_path(config.base_dir)
        self.web_prefix = config.web_prefix

        if not os.path.exists(self.base_dir):
            os.mkdir(self.base_dir)

    def save(self, provider, data_stream):
        file_path = self.file_path(provider)
        dest_path = os.path.join(
            self.base_dir,
            file_path
        )
        if not os.path.exists(os.path.dirname(dest_path)):
            os.makedirs(os.path.dirname(dest_path))
        with open(dest_path, 'wb') as outfile:
            outfile.write(data_stream.read())

        url = os.path.join(self.web_prefix, file_path)

        return {'url': url,
                'checksum_type': 'sha256',
                'checksum': self.file_hash(dest_path)}

    def file_path(self, provider):
        return os.path.join(provider.version.box.tag,
                            provider.version.version,
                            provider.name + os.path.extsep + 'box')

    def file_hash(self, file_path):
        sha256_hash = hashlib.sha256()
        with open(file_path, 'rb') as file:
            # Read and update hash string value in blocks of 4K
            for byte_block in iter(lambda: file.read(4096), b''):
                sha256_hash.update(byte_block)
            return sha256_hash.hexdigest()


def resolve_path(path):
    return os.path.realpath(os.path.expandvars(os.path.expanduser(path)))
