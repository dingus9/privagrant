import uuid

import flask
import flask_restful


class VarProxy(object):

    path_errors = []


class Api(flask_restful.Api):

    def handle_error(self, e):
        try:
            if isinstance(e, TypeError):
                return self.make_response({'message': f'{str(e)}'},
                                          400)
            if e.code == 404:
                try:
                    error = VarProxy.path_errors[0]
                    VarProxy.path_errors = []
                    return self.make_response({'message': f'404 Not Found: {str(error)}'},
                                              e.code)
                except IndexError:
                    return self.make_response({'message': str(e)}, e.code)
            elif e.code == 500:
                return self.make_response({'message': 'Internal Server Error'}, e.code)
            else:
                return self.make_response({'message': str(e)}, e.code)
        except AttributeError:
            return self.make_response({'message': f'Internal Server Error{str(e)}'},
                                      500)


class JSONEncoder(flask.json.JSONEncoder):

    def default(self, obj):
        """Add support for supported types"""
        if isinstance(obj, uuid.UUID):
            return str(obj)
        else:
            return super(JSONEncoder, self).default(obj)


class Flagz(object):
    """Represent anything as a bitwisable flag. But with style"""

    def __init__(self, *flags):
        super().__setattr__('_flags', {v: k + 1 for k, v in enumerate(flags)})
        self._flags['no_value'] = 0

        self.reset()

    def __setattr__(self, key, value):
        if key in self._flags:
            value = self._flags[key] if value else 0
            return super().__setattr__(key, value)
        else:
            raise ValueError(f'{key} is not a valid flag option')

    def bitwise(self, keys=None):
        if not keys:
            keys = self._flags.keys()

        val = 0
        for key in keys:
            val = val | getattr(self, key)

        return val

    def reset(self):
        for flag in self._flags:
            setattr(self, flag, 0)
