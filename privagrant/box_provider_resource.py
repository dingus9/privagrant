from datetime import datetime

from flask import request

from marshmallow.exceptions import ValidationError

from privagrant.resource import AbortResponseError, PrivagrantResource
from privagrant.auth import AuthError, AuthHardFail, authenticate
from privagrant.orm.schema_models import (ProviderSchema, CreateProviderSchema)
from privagrant.orm.db_models import Provider


class BoxProviderResource(PrivagrantResource):

    def get(self, username, name, version, provider):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            box = self.get_box(f'{username}/{name}')

            if box.is_private and not user.is_owner(username):
                return self.NotFound()

            dbprovider = self.get_provider(
                provider, self.get_version(version, self.get_box(f'{username}/{name}')))

            return ProviderSchema().dump(dbprovider,
                                         reset_recurse=True)

        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def delete(self, username, name, version, provider):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            # is it your username/org?
            if not user.is_owner(username):
                return self.AccessDeniedError('Not your org')

            dbprovider = self.get_provider(
                provider, self.get_version(version, self.get_box(f'{username}/{name}')))

            dbprovider.delete()
            resp = ProviderSchema().dump(dbprovider,
                                         reset_recurse=True)
            resp['deleted'] = True
            return resp

        except AuthError as e:
            return self.AuthenticationError(e)
        except ValidationError as e:
            return self.BadRequest(e)
        except Exception as e:
            return self.InternalError(f'error deleting resource {str(e)}')

    def post(self, username, name, version):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            provider_request = CreateProviderSchema().load(
                request.get_json(force=True)
            )

            # is it your username/org?
            if not user.is_owner(username):
                return self.AccessDeniedError('Not your org')

            dbversion = self.get_version(version, self.get_box(f'{username}/{name}'))

            try:
                self.get_provider(provider_request.provider.name, dbversion)
                return self.BadRequest('version exists, updating a provider not supported'
                                       ' with a POST')
            except AbortResponseError:
                pass

            now = datetime.utcnow()

            new_provider = Provider(**{
                'name': provider_request.provider.name,
                'version': dbversion,
                'url': provider_request.provider.url,
                'checksum': provider_request.provider.checksum,
                'checksum_type': provider_request.provider.checksum_type,
                'created_at': now,
                'updated_at': now
            })
            new_provider.save()

            dbversion.providers.append(new_provider)

            return ProviderSchema().dump(new_provider,
                                         reset_recurse=True), 201

        except AuthError as e:
            return self.AuthenticationError(e)
        except ValidationError as e:
            return self.BadRequest(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def put(self, username, name, version, provider):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            provider_request = CreateProviderSchema().load(
                request.get_json(force=True)
            )

            # is it your username/org?
            if not user.is_owner(username):
                return self.AccessDeniedError('Not your org')

            dbprovider = self.get_provider(
                provider,
                self.get_version(version, self.get_box(f'{username}/{name}')))

            now = datetime.utcnow()

            dbprovider.update(**{
                'name': provider_request.provider.get(
                    'name', dbprovider.name),
                'url': provider_request.provider.get(
                    'url', dbprovider.url),
                'checksum': provider_request.provider.get(
                    'checksum', dbprovider.checksum),
                'checksum_type': provider_request.provider.get(
                    'checksum_type', dbprovider.checksum_type),
                'updated_at': now
            })
            dbprovider.reload()

            return ProviderSchema().dump(dbprovider,
                                         reset_recurse=True)

        except AuthError as e:
            return self.AuthenticationError(e)
        except ValidationError as e:
            return self.BadRequest(e)
        except Exception as e:
            return self.InternalError(f'error updating resource {str(e)}')
