from attrdict import AttrDict

from marshmallow import (fields, Schema, EXCLUDE, post_dump, post_load,
                         validate, missing, ValidationError)

from marshmallow_mongoengine import ModelSchema as MMModelSchema

from privagrant.orm.db_models import Box, Provider, Version


def nested_schema(*fields):
    """Generates a schema of field objects.
        Allows for a more turse definition of nested schemas
        returns: as Schema class
    """
    fields = list(fields)
    fields.append(
        ('Meta', type('obj', (object,), {'unknown': EXCLUDE}))
    )
    schema = type('obj', (Schema,), {field[0]: field[1] for field in fields})
    return schema


class BaseSchema(Schema):
    """Schema for common defaults
    """
    class Meta:
        unknown = EXCLUDE

    @post_dump(pass_many=True)
    @post_load(pass_many=True)
    def as_attr_dict(self, data, **kwargs):
        return AttrDict(data)


class BaseDBSchema(MMModelSchema):

    def __init__(self, *args, **kwargs):
        self._reset_recurse = False
        super().__init__(*args, **kwargs)

    def dump(self, *args, **kwargs):
        if kwargs.get('reset_recurse'):
            self._reset_recurse = True
            del kwargs['reset_recurse']
        return super().dump(*args, **kwargs)

    @post_dump
    def reset_recurse(self, data, **kwargs):
        if self._reset_recurse:
            try:
                self.Meta.model.Meta.schema_map.clear()
            except AttributeError:
                self.Meta.schema_map.clear()
        return data


class LoginRequestSchema(BaseSchema):
    """Authentication request objects
    """

    user = fields.Nested(
        nested_schema(
            ('login', fields.Str(required=True)),
            ('password', fields.Str(required=True))),
        required=True)
    token = fields.Nested(
        nested_schema(
            ('description', fields.Str(required=False,
                                       default='generic_token',
                                       missing='generic_token'))
        ),
        required=False)
    two_factor = fields.Nested(
        nested_schema(
            ('code', fields.Str(default=None,
                                missing=None))
        ),
        required=False
    )


class JWTAuthResponseSchema(BaseSchema):
    """Authentication response objects
    """

    description = fields.Str()
    token_hash = fields.Str(required=True)
    token = fields.Str(required=False)
    created_at = fields.DateTime()
    user = fields.Nested(
        nested_schema(
            ('username', fields.Str(required=True))
        ),
        required=True)


class CreateProviderSchema(BaseSchema):

    provider = fields.Nested(
        nested_schema(
            ('name', fields.Str(required=True)),
            ('checksum', fields.Str()),
            ('checksum_type', fields.Str()),
            ('url', fields.Url())
        ))


class ProviderUploadSchema(BaseSchema):

    upload_path = fields.Url()
    upload_timeout = fields.Integer()


class ProviderFileUploadedSchema(BaseSchema):

    url = fields.Url()
    checksum = fields.Str()
    checksum_type = fields.Str()


class CreateVersionSchema(BaseSchema):

    version = fields.Nested(
        nested_schema(
            ('version', fields.Str(required=True)),
            ('description', fields.Str())
        ))


class UpdateVersionSchema(BaseSchema):

    version = fields.Nested(
        nested_schema(
            ('version', fields.Str(required=True)),
            ('description', fields.Str()),
            ('status', fields.Str(validate=validate.OneOf(['unreleased', 'active'])))
        ))


class CreateBoxSchema(BaseSchema):

    box = fields.Nested(
        nested_schema(
            ('username', fields.Str(required=True)),
            ('name', fields.Str(required=True)),
            ('description', fields.Str()),
            ('short_description', fields.Str()),
            ('is_private', fields.Boolean(default=False,
                                          missing=False))
        ))


class UpdateBoxSchema(BaseSchema):

    box = fields.Nested(
        nested_schema(
            ('username', fields.Str()),
            ('name', fields.Str()),
            ('description', fields.Str()),
            ('short_description', fields.Str()),
            ('is_private', fields.Boolean())
        ))


class SchemaResolver:

    _schema_map = {
        'Box': 'BoxSchema',
        'Version': 'VersionSchema',
        'Provider': 'ProviderSchema'
    }

    def __init__(self, table, max_depth=1):
        self.recurse_table = table
        self.max_depth = max_depth

    def resolve(self, model, attr, parent=None):
        if parent and self.depth(parent) == 0:
            self.count_obj(parent)
        if self.depth(model) >= self.max_depth:
            return missing
        else:
            self.count_obj(model)
            return globals()[self._schema_map[model.__class__.__name__]]

    def clear(self):
        self.recurse_table.clear()

    def depth(self, model):
        return self.recurse_table.setdefault(model.id, 0)

    def count_obj(self, obj):
        self.recurse_table[obj.id] = (
            self.recurse_table.setdefault(obj.id, 0) + 1
        )
        print(id(self), obj.__class__.__name__, obj.id, self.depth(obj))


schema_mapper = SchemaResolver(table={}, max_depth=1)


def me_to_schema(model):
    setattr(model, 'Meta', type('obj', (object,), {
        'schema_map': schema_mapper
    }))

    fields = []
    fields.append(
        ('Meta', type('obj', (object,), {
            'unknown': EXCLUDE,
            'model': model,
            'exclude': ('id',)}))
    )

    return type('obj', (BaseDBSchema,), {field[0]: field[1] for field in fields})


BoxSchema = me_to_schema(Box)
ProviderSchema = me_to_schema(Provider)
VersionSchema = me_to_schema(Version)


class BaseDBMany:

    class Meta:

        schema_map = schema_mapper


class BoxesSchema(BaseDBMany, BaseDBSchema):

    boxes = fields.List(fields.Nested(BoxSchema))


class VersionsSchema(BaseDBMany, BaseDBSchema):

    versions = fields.List(fields.Nested(VersionSchema))


class ProvidersSchema(BaseDBMany, BaseDBSchema):

    providers = fields.List(fields.Nested(ProviderSchema))


class Field_SortStr(fields.Str):

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return {'created': 'created_at',
                    'updated': 'updated_at',
                    'downloads': 'downloads'}[value]
        except KeyError:
            raise ValidationError('must be one of (created, updated, downloads)')


class Field_OrderStr(fields.Str):

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return {'asc': '+',
                    'desc': '-'}[value]
        except KeyError:
            raise ValidationError('must be one of (asc, desc)')


class SearchQuerySchema(BaseSchema):

    q = fields.Str(missing='',
                   default='')
    provider = fields.Str(default='',
                          missing='')
    sort = Field_SortStr(
        default='downloads',
        missing='downloads'
    )
    limit = fields.Int(default=25,
                       missing=25,
                       validate=validate.Range(min=1))
    order = Field_OrderStr(default='desc',
                           missing='desc')
    page = fields.Int(default=1,
                      missing=1,
                      validate=validate.Range(min=1))
