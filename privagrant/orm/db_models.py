
import mongoengine as me


DB = None


class QueryFailed(Exception):
    pass


class ZeroResults(Exception):
    pass


def db_init(db_config):
    global DB
    DB = me.connect(host=db_config.uri)


class Provider(me.Document):

    meta = {'indexes': ['name', 'version']}

    name = me.StringField(required=True, unique_with='version')
    version = me.ReferenceField('Version')
    checksum = me.StringField(required=True)
    checksum_type = me.StringField(
        required=True,
        choices=(
            ('md5', 'md5'),
            ('sha256', 'sha256'),
            ('sha1', 'sha1'),
            ('sha384', 'sha384'),
            ('sha512', 'sha512'))
    )
    url = me.URLField()
    upload_path = me.URLField()
    upload_object_id = me.UUIDField()
    upload_timeout = me.IntField()
    upload_enabled_at = me.DateTimeField()
    hosted = me.BooleanField(default=False)
    hosted_token = me.StringField()
    created_at = me.DateTimeField(required=True)
    updated_at = me.DateTimeField(required=True)


class Version(me.Document):

    meta = {'indexes': ['version', 'box', 'status', 'providers']}

    version = me.StringField(required=True, unique_with='box')
    status = me.StringField(
        choices=(('unreleased', 'unreleased'),
                 ('active', 'active')),
        default='unreleased'
    )
    created_at = me.DateTimeField(required=True)
    updated_at = me.DateTimeField(required=True)
    description = me.StringField()
    box = me.ReferenceField('Box')
    providers = me.ListField(me.ReferenceField(Provider))


class Box(me.Document):

    meta = {'indexes': ['tag', 'username', 'name', 'is_private']}

    tag = me.StringField(required=True, unique=True)
    username = me.StringField(required=True)
    name = me.StringField(required=True)
    is_private = me.BooleanField(required=True, default=False)
    downloads = me.IntField(required=True, default=0)
    created_at = me.DateTimeField(required=True)
    updated_at = me.DateTimeField(required=True)
    short_description = me.StringField()
    description = me.StringField()
    current_version = me.ReferenceField(Version)
    versions = me.ListField(me.ReferenceField(Version))
