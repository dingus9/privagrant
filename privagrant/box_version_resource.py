from datetime import datetime

from flask import request

from marshmallow.exceptions import ValidationError

from privagrant.resource import AbortResponseError, PrivagrantResource
from privagrant.auth import AuthError, AuthHardFail, authenticate
from privagrant.orm.schema_models import (CreateVersionSchema, UpdateVersionSchema,
                                          VersionSchema, VersionsSchema)
from privagrant.orm.db_models import Version


class BoxVersionResource(PrivagrantResource):

    def get(self, username, name, version=None):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            box = self.get_box(f'{username}/{name}')

            if box.is_private and not user.is_owner(username):
                return self.NotFound()
            if version:
                return VersionSchema().dump(self.get_version(version, box),
                                            reset_recurse=True)

            else:
                return VersionsSchema().dump({'versions': self.get_versions(box)},
                                             reset_recurse=True)

        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def delete(self, username, name, version):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            if not user.is_owner(username):
                return self.NotFound()

            box = self.get_box(f'{username}/{name}')
            version = self.get_version(version, box)
            version.delete()
            resp = VersionSchema().dump(version,
                                        reset_recurse=True)
            resp['deleted'] = True
            return resp
        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def post(self, username, name):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            version_request = CreateVersionSchema().load(
                request.get_json(force=True)
            )

            # is it your username/org?
            if not user.is_owner(username):
                return self.AccessDeniedError('Not your org')

            box = self.get_box(f'{username}/{name}')

            try:
                self.get_vesion(version_request.version.version, box)
                return self.BadRequest('version exists, updating a version not supported'
                                       ' with a POST')
            except AbortResponseError:
                pass

            now = datetime.utcnow()

            new_version = Version(**{
                'version': version_request.version.version,
                'description': version_request.version.description,
                'status': 'unreleased',
                'box': box,
                'created_at': now,
                'updated_at': now
            })
            new_version.save()
            return VersionSchema().dump(new_version,
                                        reset_recurse=True), 201

        except AuthError as e:
            return self.AuthenticationError(e)
        except ValidationError as e:
            return self.BadRequest(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def put(self, username, name, version):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            version_request = UpdateVersionSchema().load(
                request.get_json(force=True)
            )

            tag = f'{username}/{name}'
            if not user.is_owner(username):
                return self.NotFound(f'box {tag}/version/{version}')

            box = self.get_box(tag)

            dbversion = self.get_version(version, box)

            now = datetime.utcnow()
            if version_request.version.get('status'):
                from privagrant.app import api
                release_path = api.url_for(BoxVersionReleaseResource,
                                           username=username,
                                           name=name,
                                           version=version)
                return self.BadRequest(
                    f'you must call PUT {release_path} to release a version')

            dbversion.update(**{
                'updated_at': now,
                'version': version_request.version.get(
                    'version', version.version),
                'description': version_request.version.get(
                    'description', version.description)
            })
            dbversion.reload()
            return VersionSchema().dump(dbversion,
                                        reset_recurse=True)

        except AuthError as e:
            return self.AuthenticationError(e)
        except ValidationError as e:
            return self.BadRequest(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(f'error updating resource {str(e)}')


class BoxVersionReleaseResource(PrivagrantResource):

    def put(self, username, name, version):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            tag = f'{username}/{name}'
            if not user.is_owner(username):
                return self.NotFound(f'box {tag}/version/{version}')

            box = self.get_box(tag)

            dbversion = self.get_version(version, box)

            now = datetime.utcnow()

            if not len(dbversion.providers):
                return self.BadRequest('You must create a valid provider before '
                                       'releasing a new version.')

            dbversion.update(**{
                'updated_at': now,
                'status': 'active'
            })
            box.current_version = dbversion
            box.save()
            dbversion.reload()
            return VersionSchema().dump(dbversion,
                                        reset_recurse=True)

        except AuthError as e:
            return self.AuthenticationError(e)
        except ValidationError as e:
            return self.BadRequest(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(f'error updating resource {str(e)}')
