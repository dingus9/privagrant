from os import path
from importlib import import_module

import yaml

from attrdict import AttrDict

from privagrant.storage.local import LocalStorage


class ConfigNotLoaded(Exception):
    pass


class ConfigValidationError(Exception):
    pass


class AuthStack:

    stack = []
    methods = {}


class StorageBackend:

    methods = {
        'local': LocalStorage
    }

    active = None


class PrivagrantConfig:

    _cfg = None

    @classmethod
    def config(cls):
        if cls._cfg is None:
            raise ConfigNotLoaded()
        else:
            return cls._cfg

    @classmethod
    def load_config(cls, config_file):
        file = path.abspath(path.expanduser(path.expandvars(config_file)))
        with open(file) as conf_file:
            cls._cfg = AttrDict(yaml.safe_load(conf_file))

        cls.load_auth_plugins()

        cls.load_storage_backend()

        return cls

    @classmethod
    def load_auth_plugins(config_cls):
        auth_plugins = {}
        for auth_plugin in config_cls._cfg.app.auth_methods:
            if auth_plugin.name in auth_plugins:
                raise ConfigValidationError('auth plugin defined more then once')

            plugin = getattr(import_module(auth_plugin.module),
                             auth_plugin.name)
            auth_plugins[auth_plugin.name] = plugin(
                auth_plugin.config,
                getattr(auth_plugin, 'forward_to', None)
            )

        # check for infinate forward_to loop
        for name, cls in auth_plugins.items():
            i = 0
            next_cls = cls
            while next_cls.forward_to:
                if i >= len(auth_plugins):
                    raise ConfigValidationError('infinate auth forward_to loop')
                next_cls = auth_plugins[next_cls.forward_to]
                i += 1

        AuthStack.methods.update(auth_plugins)

    @classmethod
    def load_storage_backend(config_cls):
        provider_upload_config = config_cls._cfg.app.provider_upload
        storage_config = getattr(provider_upload_config, provider_upload_config.provider)
        StorageBackend.active = StorageBackend.methods[
            provider_upload_config.provider
        ](storage_config)

    @classmethod
    def set_config(cls, config):
        cls._cfg = config

        return cls
