from privagrant.orm.db_models import QueryFailed
from privagrant.resource import PrivagrantResource


from marshmallow import EXCLUDE, Schema, fields


class RespSchema(Schema):

    status = fields.Str()
    timestamp = fields.DateTime()
    # request_id = fields.UUID()

    class Meta:
        unknown = EXCLUDE


class HealthCheckResource(PrivagrantResource):

    def get(self):
        try:
            pass
            # Do queries here to check db and other things
        except QueryFailed:
            return 'unknown error', 500
