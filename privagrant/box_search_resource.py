from flask import request

from marshmallow.exceptions import ValidationError

from mongoengine.queryset.visitor import Q

from privagrant.resource import AbortResponseError, PrivagrantResource
from privagrant.auth import AuthError, AuthHardFail, authenticate
from privagrant.orm.schema_models import BoxesSchema, SearchQuerySchema
from privagrant.orm.db_models import Box, Provider, Version


class BoxSearchResource(PrivagrantResource):

    def get(self):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            args = SearchQuerySchema().load(request.args)

            box_query = {'current_version__exists': True}

            if args.q:
                box_query['name__icontains'] = args.q

            public_query = {'is_private': False}

            # current users boxes query
            private_query = {'is_private': True,
                             'username': user.username_claim}

            page = args.page - 1

            if args.provider:
                providers = Provider.objects(name=args.provider)
                with_providers = Version.objects(
                    id__in=(p.id for p in providers.no_dereference().scalar('version')))

                box_query['id__in'] = [b.id for b
                                       in with_providers.no_dereference().scalar('box')]

            boxes = Box.objects(
                (Q(**box_query) & Q(**private_query)
                 | Q(**box_query) & Q(**public_query))
            ).order_by(f'{args.order}{args.sort}')[page:args.limit]

            return BoxesSchema().dump({'boxes': boxes},
                                      reset_recurse=True)

        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except ValidationError as e:
            return self.BadRequest(str(e))
        except Exception as e:
            return self.InternalError(str(e))
