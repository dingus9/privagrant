from privagrant.resource import PrivagrantResource
from privagrant.auth import AuthError, AuthHardFail, authenticate, login
from privagrant.orm.schema_models import JWTAuthResponseSchema


class AuthenticateResource(PrivagrantResource):

    def get(self):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            schema = JWTAuthResponseSchema()
            return schema.dump(
                schema.load({
                    'description': user.jwt_description,
                    'token': '',
                    'token_hash': user.jwt_hash,
                    'created_at': user.iat,
                    'user': {'username': user.username_claim}
                }))
        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)

    def post(self):  # noqa: C901
        try:
            user = login()

            if not user.is_authenticated():
                raise AuthError('login failed')

            schema = JWTAuthResponseSchema()
            return schema.dump(
                schema.load({
                    'description': user.jwt_description,
                    'token': user.jwt,
                    'token_hash': user.jwt_hash,
                    'created_at': user.iat,
                    'user': {'username': user.username_claim}
                }))
        except AuthError as e:
            return self.AuthenticationError(e)
        except Exception as e:
            return self.InternalError(str(e))
