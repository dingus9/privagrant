import json
import os

import privagrant.auth_resource as auth

import privagrant.health_resource as health

from privagrant import (
    box_provider_resource,
    box_provider_upload_resource,
    box_resource,
    box_search_resource,
    box_version_resource
)

from attrdict import AttrDict
from flask import Flask

from privagrant.config import PrivagrantConfig
from privagrant.orm.db_models import db_init
from privagrant.util import Api, JSONEncoder

app = Flask('privagrant')

app.config.setdefault('RESTFUL_JSON', {})['cls'] = app.json_encoder = JSONEncoder

api = Api(app, catch_all_404s=True)

api.add_resource(auth.AuthenticateResource,
                 '/api/v1/authenticate',
                 '/authenticate')

api.add_resource(health.HealthCheckResource,
                 '/health')

api.add_resource(box_resource.BoxResource,
                 '/api/v1/boxes',
                 '/api/v1/boxes/<string:username>',
                 '/api/v1/box/<string:username>/<string:name>')

api.add_resource(box_version_resource.BoxVersionResource,
                 '/api/v1/box/<string:username>/<string:name>/versions',
                 '/api/v1/box/<string:username>/<string:name>/version/<string:version>')

api.add_resource(box_version_resource.BoxVersionReleaseResource,
                 '/api/v1/box/<string:username>/'
                 '<string:name>/version/<string:version>/release')

api.add_resource(
    box_provider_resource.BoxProviderResource,
    '/api/v1/box/<string:username>/<string:name>/version/<string:version>/providers',
    '/api/v1/box/<string:username>/<string:name>'
    '/version/<string:version>/provider/<string:provider>'
)

api.add_resource(
    box_provider_upload_resource.BoxProviderUploadResource,
    '/api/v1/box/<string:username>/<string:name>'
    '/version/<string:version>/provider/<string:provider>/upload',
    '/api/v1/object/<uuid:object_id>'
)


api.add_resource(box_search_resource.BoxSearchResource,
                 '/search')


def app_setup(config_path=None, config=None):
    assert config_path or config, 'No application config specified'
    assert not (config_path and config), 'Specify either a config file or dict'

    if config_path:
        config = PrivagrantConfig.load_config(config_path).config()
    else:
        config = PrivagrantConfig.set_config(config).config()

    assert isinstance(config, AttrDict), 'Config is not an AttrDict object'

    db_init(config.app.database)  # Initialize the db (Config.database for config)
    return config


def run_uwsgi(env, start_response):
    # preload config options from environment json string
    config = AttrDict(json.loads(os.environ.get('APP_CONFIG', '{}')))
    assert 'database' in config
    app_setup(config=config)
    return app(env, start_response)


def run_dev(config_path=None, config=None):  # pragma: no cover
    config = app_setup(config_path=config_path)
    app.run(**config.server)


#: Configured routes for application
routes = sorted([str(x) for x in app.url_map.iter_rules()])
