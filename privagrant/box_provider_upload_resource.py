from datetime import datetime
from flask import request

from privagrant.auth import AuthError, AuthHardFail, authenticate
from privagrant.resource import AbortResponseError, PrivagrantResource
from privagrant.orm.schema_models import (ProviderUploadSchema,
                                          ProviderSchema,
                                          ProviderFileUploadedSchema)
from privagrant.config import PrivagrantConfig, StorageBackend


class BoxProviderUploadResource(PrivagrantResource):

    def get(self, username, name, version, provider):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            box = self.get_box(f'{username}/{name}')

            if box.is_private and not user.is_owner(username):
                return self.NotFound()

            dbprovider = self.get_provider(
                provider, self.get_version(version, self.get_box(f'{username}/{name}')))

            object_id, upload_path = self.get_upload_path(dbprovider)

            if not PrivagrantConfig.config().app.provider_upload.enabled:
                return ProviderUploadSchema().dump({'upload_path': ''})

            timeout = PrivagrantConfig.config().app.provider_upload.upload_url_timeout
            now = datetime.utcnow()
            dbprovider.update(
                **{'upload_path': upload_path,
                   'upload_timeout': timeout,
                   'upload_object_id': object_id,
                   'upload_enabled_at': now,
                   'updated_at': now}
            )
            dbprovider.reload()
            return ProviderUploadSchema().dump(dbprovider)

        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def delete(self, username, name, version, provider):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            box = self.get_box(f'{username}/{name}')

            if box.is_private and not user.is_owner(username):
                return self.NotFound()

            dbprovider = self.get_provider(
                provider, self.get_version(version, self.get_box(f'{username}/{name}')))

            dbprovider.update(
                **{'upload_path': None,
                   'upload_timeout': None,
                   'upload_object_id': None,
                   'upload_enabled_at': None,
                   'updated_at': datetime.utcnow()}
            )
            dbprovider.reload()
            return ProviderSchema().dump(dbprovider,
                                         reset_recurse=True)

        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def put(self, object_id):
        config = PrivagrantConfig.config()
        try:
            provider = self.get_provider_object_id(object_id=object_id)
            if not provider.upload_path:
                return self.NotFound()

            now = datetime.utcnow()
            timeout = (
                provider.upload_timeout if provider.upload_timeout
                else config.app.provider_upload.upload_url_timeout)

            if now.timestamp() > provider.upload_enabled_at.timestamp() + timeout:
                raise AbortResponseError(self.BadRequest('upload url has expired'))

            upload_data = StorageBackend.active.save(provider, request.stream)
            upload_data.update({
                'upload_path': None,
                'upload_timeout': None,
                'upload_object_id': None,
                'upload_enabled_at': None,
                'updated_at': datetime.utcnow()
            })
            provider.update(**upload_data)

            return ProviderFileUploadedSchema().dump(upload_data)

        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))
