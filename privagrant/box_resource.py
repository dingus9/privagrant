from datetime import datetime

from flask import request

from marshmallow.exceptions import ValidationError

from privagrant.resource import AbortResponseError, PrivagrantResource
from privagrant.auth import AuthError, AuthHardFail, authenticate
from privagrant.orm.schema_models import (BoxSchema, BoxesSchema,
                                          CreateBoxSchema, UpdateBoxSchema)
from privagrant.orm.db_models import Box


class BoxResource(PrivagrantResource):

    def get(self, username, name=None):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            if name:
                tag = f'{username}/{name}'
                box = self.get_box(tag)

                if box.is_private and not user.is_owner(username):
                    return self.NotFound(f'box {tag}')

                return BoxSchema().dump(box,
                                        reset_recurse=True)
            else:
                return BoxesSchema().dump({
                    'boxes': self.get_boxes(
                        username,
                        include_private=(username == user.username_claim)
                    )},
                    reset_recurse=True
                )

        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def delete(self, username, name):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            tag = f'{username}/{name}'

            if not user.is_owner(username):
                return self.NotFound(f'box {tag}')

            box = self.get_box(tag)
            box.delete()
            box_resp = BoxSchema().dump(box,
                                        reset_recurse=True)
            box_resp['deleted'] = True
            return box_resp
        except (AuthHardFail, AuthError) as e:
            return self.AuthenticationError(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(str(e))

    def post(self):
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            box_request = CreateBoxSchema().load(
                request.get_json(force=True)
            )

            # is it your username/org?
            if not user.is_owner(box_request.box.username):
                return self.AccessDeniedError('Not your org')

            tag = f'{box_request.box.username}/{box_request.box.name}'

            try:
                self.get_box(tag)
                return self.BadRequest('box exists, updating a box not supported'
                                       ' with a POST')
            except AbortResponseError:
                pass

            now = datetime.utcnow()

            new_box = Box(**{
                'tag': tag,
                'name': box_request.box.name,
                'username': box_request.box.username,
                'short_description': box_request.box.short_description,
                'description': box_request.box.description,
                'is_private': box_request.box.is_private,
                'created_at': now,
                'updated_at': now,

            })
            new_box.save()
            return BoxSchema().dump(new_box,
                                    reset_recurse=True), 201
        except AuthError as e:
            return self.AuthenticationError(e)
        except ValidationError as e:
            return self.BadRequest(e)
        except Exception as e:
            return self.InternalError(str(e))

    def put(self, username, name):  # noqa: C901
        try:
            user = authenticate()

            if not user.is_authenticated():
                raise AuthError('login failed')

            box_request = UpdateBoxSchema().load(
                request.get_json(force=True)
            )

            tag = f'{username}/{name}'

            if not user.is_owner(username):
                return self.NotFound(f'box {tag}')

            box = self.get_box(tag)

            if box_request.box.get('username') and box_request.box.username != username:
                return self.BadRequest('Changing username/org not supported')

            now = datetime.utcnow()
            new_name = box_request.box.get('name', name)
            new_tag = f'{username}/{new_name}'

            # Check new tag to avoid clobbering
            if new_tag != f'{username}/{name}':
                try:
                    self.get_box(new_tag)
                    return self.BadRequest(f'another box exists at {new_tag}')
                except AbortResponseError:
                    pass

            box.update(**{
                'tag': new_tag,
                'updated_at': now,
                'name': new_name,
                'is_private': box_request.box.get(
                    'is_private', box.is_private),
                'short_description': box_request.box.get(
                    'short_description', box.short_description),
                'description': box_request.box.get(
                    'description', box.description)
            })
            box.reload()
            return BoxSchema().dump(box,
                                    reset_recurse=True)

        except AuthError as e:
            return self.AuthenticationError(e)
        except ValidationError as e:
            return self.BadRequest(e)
        except AbortResponseError as e:
            return e.response
        except Exception as e:
            return self.InternalError(f'error updating resource {str(e)}')
