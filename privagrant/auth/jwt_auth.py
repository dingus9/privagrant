import hashlib

from datetime import datetime, timezone

from attrdict import AttrDict

from authlib.jose import jwt, JWTClaims
from authlib.jose.errors import InvalidClaimError, ExpiredTokenError
from flask import request
from marshmallow import fields

from privagrant.auth.base_auth import AuthError, AuthHardFail, BaseAuth
from privagrant.orm.schema_models import BaseSchema


class JWTAuth(BaseAuth):

    class Config(BaseSchema):
        algorithm = fields.Str(required=True,
                               )
        secret = fields.Str(required=False)
        valid_within = fields.Integer(required=False,
                                      default=18000,
                                      missing=18000)
        uid_claim = fields.Str(required=True)
        group_claim = fields.Str(required=True)
        username_claim = fields.Str(required=True)

    def __init__(self, config, forward_to):
        super().__init__(config, forward_to)

    def authenticate(self, auth_chain):
        token = request.args.get('access_token', None)
        if not token:
            auth_header = request.headers.get('Authorization', '')
            if 'bearer' not in auth_header.lower():
                raise AuthError('no valid bearer auth header')
            token = auth_header[7:]

        claims_opts = {
            'exp': {
                'essential': True
            },
            self.config.group_claim: {
                'essential': True,
            }
        }
        try:
            claims = jwt.decode(token, self.config.secret, JWTClaims, claims_opts)
            claims.validate()
            iat = datetime.fromtimestamp(claims.get('iat', 0))
            exp = datetime.fromtimestamp(claims.get('exp', 0))
            return {
                'jwt_description': claims.get('description', ''),
                'username_claim': claims.get(self.config.username_claim, 'anonymous'),
                'uid_claim': claims.get(self.config.uid_claim, ''),
                'group_claim': claims.get(self.config.group_claim, ''),
                'iat': iat.astimezone(tz=timezone.utc).isoformat(),
                'exp': exp.astimezone(tz=timezone.utc).isoformat(),
                'jwt_hash': hashlib.sha256(token.encode()).hexdigest()
            }
        except (InvalidClaimError, ExpiredTokenError) as e:
            if 'exp' in e.description:
                raise AuthHardFail('expired token')
            else:
                raise AuthHardFail('bad token')

    def login(self, auth_chain):
        user = self.auth_chain_flatten(auth_chain)
        if not self.username_claim(user):
            raise AuthError('no valid authenticated username_claim')
        else:
            header = {'alg': self.config.algorithm}
            payload = {
                'description': self.description(user),
                self.config.username_claim: self.username_claim(user),
                self.config.uid_claim: self.uid_claim(user),
                self.config.group_claim: self.group_claim(user),
                'iat': self.iat(user),
                'exp': self.exp(user)
            }
            jwt_val = jwt.encode(header,
                                 payload,
                                 self.config.secret).decode('utf8')

            return AttrDict({'jwt': jwt_val,
                             'jwt_hash': hashlib.sha256(jwt_val.encode()).hexdigest(),
                             'jwt_description': self.description(user)
                             })

    def username_claim(self, user):
        if 'username_claim' not in user:
            raise AuthError('no username_claim found')
        return user.username_claim

    def group_claim(self, user):
        if 'group_claim' not in user:
            raise AuthError('no group_claim found')
        return user.group_claim

    def uid_claim(self, user):
        if 'uid_claim' not in user:
            raise AuthError('no uid_claom found')
        return user.uid_claim

    def iat(self, user):

        if 'iat' in user:
            return int(datetime.fromisoformat(user.iat).timestamp())
        else:
            return int(datetime.now(tz=timezone.utc).timestamp())

    def exp(self, user):
        return self.iat(user) + self.config.valid_within

    def description(self, user):
        return user.get('description', '')
