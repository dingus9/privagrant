from datetime import datetime, timezone
import yaml

from flask import request
from marshmallow import fields, ValidationError

from privagrant.auth.base_auth import BaseAuth, AuthError
from privagrant.orm.schema_models import (BaseSchema,
                                          LoginRequestSchema,
                                          nested_schema)


class LocalUsersAuth(BaseAuth):

    class Config(BaseSchema):
        local_users_yaml = fields.Str(required=True)
        username_claim = fields.Str(required=True)
        uid_claim = fields.Str(required=True)
        group_claim = fields.Str(required=True)
        username_claim = fields.Str(required=True)

    class UsersSchema(BaseSchema):
        users = fields.List(
            fields.Nested(
                nested_schema(
                    ('username', fields.Str(required=True)),
                    ('password', fields.Str(required=True)),
                    ('groups', fields.List(fields.Str(), required=True))
                ),
                required=True
            )
        )

    def __init__(self, config, forward_to):
        super().__init__(config, forward_to)
        with open(self.config.local_users_yaml, 'r') as yamfile:
            users = yaml.safe_load(yamfile)
        self.users = self.UsersSchema().load(users)

    def login(self, auth_chain):
        try:
            auth_body = LoginRequestSchema().load(
                request.get_json(force=True))
            for user in self.users.users:
                if (self.username_claim(user) == auth_body.user.login
                   and user.password == auth_body.user.password):
                    return {
                        'username_claim': self.username_claim(user),
                        'group_claim': self.group_claim(user),
                        'uid_claim': self.uid_claim(user),
                        'iat': self.iat(user),
                        'description': self.description(auth_body)
                    }
            raise AuthError(['No matched logins'])
        except ValidationError as e:
            raise AuthError(e.errors)

    def username_claim(self, user):
        return user[self.config.username_claim]

    def group_claim(self, user):
        return user[self.config.group_claim]

    def uid_claim(self, user):
        return user[self.config.uid_claim]

    def iat(self, user):
        return datetime.now(tz=timezone.utc).isoformat()

    def description(self, user):
        return user.get('token', {}).get('description', '')
