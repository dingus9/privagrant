from attrdict import AttrDict


class AuthError(Exception):
    pass


class AuthHardFail(Exception):
    pass


class BaseAuth:

    def __init__(self, config, forward_to=None):

        self.forward_to = forward_to

        if self.Config:
            self.config = self.Config().load(config)
        else:
            self.config = config

    def auth_chain_flatten(self, auth_chain):
        user = {}
        for item in auth_chain:
            user.update(item)
        return AttrDict(user)
