from attrdict import AttrDict
from flask import g

from privagrant.config import AuthStack

from privagrant.auth.base_auth import AuthError, AuthHardFail


def authenticate():
    run_auth('authenticate')
    return get_authed_user()


def login():
    run_auth('login')
    return get_authed_user()


def run_auth(method):

    if not hasattr(g, 'auth_chain'):
        g.auth_chain = []

    for name, auth in AuthStack.methods.items():
        next_auth = auth
        while next_auth:
            if name in [item['auth_method'] for item in g.auth_chain]:
                pass  # already ran succeeded as part of a forward_to chain
            try:
                auth_resp = getattr(next_auth, method)(g.auth_chain)
                auth_resp['auth_method'] = name
                g.auth_chain.append(auth_resp)
            except AuthError:  # didn't auth successfully but try another
                pass
            except AttributeError:  # doesn't implement authenticate
                pass

            next_auth = AuthStack.methods.get(getattr(next_auth, 'forward_to', None),
                                              None)


class User:

    def __init__(self, auth_chain):
        self.auth_chain = AttrDict(auth_chain)
        if not isinstance(self.auth_chain.get('group_claim'), (list, tuple)):
            raise AuthError('authentication failed, no user found')

    def is_owner(self, resource_owner):
        if not resource_owner:
            return False

        if resource_owner == self.auth_chain.get('username_claim', ''):
            return True
        elif resource_owner in self.auth_chain.get('group_claim', []):
            return True
        else:
            return False

    def is_authenticated(self):
        return self.auth_chain.get('username_claim') is not None

    def __getattribute__(self, key):
        try:
            return super().__getattribute__(key)
        except Exception:
            return getattr(self.auth_chain, key)


def get_authed_user(fields=('username_claim', 'jwt', 'jwt_hash', 'jwt_description',
                            'group_claim', 'uid_claim', 'iat')):
    assert fields is not None, 'fields is required to be a tuple or list of strings'
    auth_chain = {}
    for field in fields:
        for claims in g.auth_chain:
            if field in claims:
                auth_chain[field] = claims[field]
    return User(auth_chain)
