import uuid

from flask_restful import Resource, request

from privagrant.orm.db_models import (Box, Provider, Version)

from mongoengine.queryset.visitor import Q


class AbortResponseError(Exception):

    def __init__(self, response):
        self.response = response


class PrivagrantResource(Resource):

    def BadRequest(self, message=None):
        message = message if message else ''
        return ({'message':
                 f'Bad Request - Could not execute based on: {message}'},
                400)

    def NotFound(self, message=None):
        message = f': {message}' if message else ''
        return ({'message': f'Not Found{message}'}, 404)

    def InternalError(self, message=None):
        message = message if message else ''
        return ({'message': f'Internal Server Error: {message}'}, 500)

    def AuthenticationError(self, message=None):
        message = f': {message}' if message else ''
        code = 401 if 'expired' in message else 403
        return ({'message': f'Authentication denied: {message}'}, code)

    def AccessDeniedError(self, message=None):
        message = f': {message}' if message else ''
        code = 404 if 'denied' in message else 403
        return ({'message': f'Not found: {message}'}, code)

    def get_box(self, tag):
        box_query = Box.objects(tag=tag)

        if len(box_query):
            return box_query[0]
        else:
            raise AbortResponseError(self.NotFound(f'box {tag}'))

    def get_boxes(self, username, include_private=False):

        query = Q(username=username, is_private=False)

        if include_private:
            query = query | Q(username=username, is_private=True)
        return Box.objects(query)

    def get_version(self, version, box):
        version_query = Version.objects(version=version,
                                        box=box)
        if len(version_query):
            return version_query[0]
        else:
            raise AbortResponseError(self.NotFound(
                f'version {box.tag}/version/{version}'))

    def get_versions(self, box):
        version_query = Version.objects(box=box)

        if len(version_query):
            return version_query
        else:
            raise AbortResponseError(self.NotFound(
                f'version {box.tag}/versions'))

    def get_provider(self, name, version):
        provider_query = Provider.objects(name=name,
                                          version=version)

        if len(provider_query):
            return provider_query[0]
        else:
            raise AbortResponseError(self.NotFound(
                f'provider {version.box.tag}/version/{version.version}/provider/{name}'))

    def get_provider_object_id(self, object_id):
        provider_query = Provider.objects(
            upload_object_id=object_id)

        if len(provider_query):
            return provider_query[0]
        else:
            raise AbortResponseError(self.NotFound(
                f'provider for object upload with {object_id}'))

    def get_upload_path(self, provider):
        # resources must be imported after application has been insatiated...
        # also this avoids recursive imports
        from privagrant.app import api
        from privagrant.box_provider_upload_resource import BoxProviderUploadResource
        object_id = str(uuid.uuid4())
        upload_path = api.url_for(BoxProviderUploadResource, object_id=object_id)
        return object_id, f'{request.host_url.rstrip("/")}{upload_path}'
